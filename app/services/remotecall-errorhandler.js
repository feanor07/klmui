import Ember from 'ember';

export default Ember.Service.extend({
  handle(error) {
    console.log(error);
    alert("There occured a problem while getting response from server; please try again later");
  }
});
