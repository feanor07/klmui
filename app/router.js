import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('klmfare');
  this.route('statistics');
  this.route('airports');
});

export default Router;
