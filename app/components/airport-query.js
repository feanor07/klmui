import Ember from 'ember';
var baseUri = "http://localhost:9000/travel/api";

export default Ember.Component.extend({
  tableClassNames:'table table-striped table-bordered table-hover table-responsive table-condensed',

  init() {
    this._super(...arguments);
    this.set('page', 1);
    this.set('pageSize', 10);
    this.set('data', Ember.A());
    this.set('term', '');
    this.set('sortingEnabled', false);
  },

  query: function () {
    let sortingPath = this.get('sortingEnabled') === true ? "/namesorted" : "";
    
    let url = `${baseUri}/airports${sortingPath}?term=${this.get('term')}&lang=${this.get('lang')}&size=${
      this.get('pageSize')}&page=${this.get('page')}`;
    Ember.$.get(url).success((json)=>this.set('data', json)).fail((error)=>this.get('remotecallErrorhandler').handle(error));
  },

  actions:{
    selectionChanged:function(selectedRows){
      this.set('selectedRows', selectedRows);
    },

    dataRequested:function(offset, limit) {
      this.set('page', (offset/limit)+1);
      this.query();
    },

    search() {
      this.set('page', 1);
      this.query();
    }
  }
});
