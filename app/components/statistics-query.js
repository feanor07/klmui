import Ember from 'ember';
var baseUri = "http://localhost:9000/travel/api/statistics";
export default Ember.Component.extend({
  remotecallErrorhandler: Ember.inject.service(),

  init() {
    this._super(...arguments);
    this.doRefreshStatistics();
  },

  doRefreshStatistics: function() {
    Ember.$.get(baseUri).success((json)=>this.set('statisticsDto', json)).fail(
      (error)=>this.get('remotecallErrorhandler').handle(error));
  },

  actions: {
    refresh:function () {
      this.doRefreshStatistics();
    }
  }
});
