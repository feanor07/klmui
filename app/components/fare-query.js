import Ember from 'ember';
import { task, timeout } from 'ember-concurrency';
var baseUri = "http://localhost:9000/travel/api";
export default Ember.Component.extend({

  remotecallErrorhandler: Ember.inject.service(),

  init() {
    this._super(...arguments);
    this.set('currencies', ['EUR', 'USD']);
    this.set('langs', ['en', 'nl']);
    this.set('currency', 'EUR');
    this.set('lang', 'en');
    this.set('isLoading', false);
  },

  fareInfoExists: Ember.computed.notEmpty('fareInfo'),

  buttonDisabled:Ember.computed('origin', 'destination', function(){
    return Ember.isBlank(this.get('origin')) || Ember.isBlank(this.get('destination')) ||
      this.get('origin.code') === this.get('destination.code');
  }),

  searchAirport: task(function* (term) {
    yield timeout(500);
    let url = `${baseUri}/airports?term=${term}&lang=${this.get('lang')}`;

    return Ember.$.get(url).fail((error)=>this.get('remotecallErrorhandler').handle(error));
  }),

  actions: {
    getPrice:function () {
      let originCode = this.get('origin.code');
      let destinationCode = this.get('destination.code');
      this.set('isLoading', true);
      let url = `${baseUri}/fares/${originCode}/${destinationCode}?currency=${this.get('currency')}&lang=${this.get('lang')}`;
      Ember.$.get(url).success((json)=>{this.set('fareInfo', json); this.set('isLoading', false);}).
        fail((error)=>{this.get('remotecallErrorhandler').handle(error); this.set('isLoading', false);});
    }
  }
});
